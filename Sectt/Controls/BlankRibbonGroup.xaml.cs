﻿using Fluent;

namespace Sectt.Ultilities.Controls
{
    /// <summary>
    /// Interaction logic for BlankRibbonGroup.xaml
    /// </summary>
    public partial class BlankRibbonGroup : RibbonGroupBox
    {
        public BlankRibbonGroup()
        {
            InitializeComponent();
        }
    }
}
